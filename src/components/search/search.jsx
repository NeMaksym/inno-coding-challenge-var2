import React, { Component } from 'react';

import './search.css';

class Search extends Component {
    
    onSearchChanged = (evt) => {
        this.props.onSearchChanged(evt.target.value)
    }

    render() {
        const {searchValue} = this.props;
        
        return (
            <form action="">
                <input type = "search"
                       placeholder = "Search by name"
                       onChange = {this.onSearchChanged}
                       value = {searchValue}/>
            </form>
        )
    }
}

export default Search;