import React, { Component } from 'react';

import Spiner from '../spiner';

import './artists-table.css';

class ArtistsTable extends Component {

    state = {
        currentPage: 1
    }

    onIdClick = (id) => {
        const {onIdClick} = this.props;
        onIdClick(id);
    }

    onPaginatorClick = (event) => {
        this.setState({
            currentPage: +event.target.textContent
        })
    }

    spliceArtistForPages = (fullList) => {
        const {currentPage} = this.state;

        const content = fullList.map( (item) => {
            return (
                <tr key = {item.artist.artist_id}
                    className="table-content__artist">
                    <td onClick = {() => this.onIdClick(item.artist.artist_id)}>{item.artist.artist_id}</td>
                    <td>{item.artist.artist_name}</td>
                    <td>{item.artist.artist_country}</td>
                    <td>{item.artist.artist_rating}</td>
                    <td>{item.artist.updated_time}</td>
                </tr>
            )
        })

        if (currentPage === 1) {
            return content.slice(0, 10);
        } else {
            return content.slice(11)
        }
        // Yes, it WILL BE refactored once I have more time
    }

    render() {
        const {artistsList} = this.props;
        
        if (!artistsList) {
            return <Spiner />
        }

        return (
            <>
                <table className="table-content">
                    <tbody>
                        <tr className="table-content__header">
                            <td>ID</td>
                            <td>Name</td>
                            <td>Country</td>
                            <td>Rating</td>
                            <td>Uploaded At</td>
                        </tr>
                        {this.spliceArtistForPages(artistsList)}
                    </tbody>
                </table>
                <div className = "paginator">
                    <button onClick = {this.onPaginatorClick}>1</button>
                    <button onClick = {this.onPaginatorClick}>2</button>
                </div>
            </>
        )
    }
}

export default ArtistsTable;