import React, { Component } from 'react';

import Search from '../search';
import ArtistsTable from '../artists-table';
import ModalWindow from '../modal-window';

import './app.css';
import ArtistsData from './MusicArtists.json';


class App extends Component {
  constructor() {
    super();
    this.state = {
      artistList: null,

      idChosen: null,
      showModal: false, 

      searchValue: ''
    }

    this.setArtistToStorage();
  }
  

  componentDidMount() {
    this.setState({
      artistList: this.getArtistsFromStorage()
    })
  }
  
  setArtistToStorage() {
    let string = JSON.stringify(ArtistsData.artist_list);
    localStorage.setItem('artistList', string);
  }

  getArtistsFromStorage() {
    return JSON.parse(localStorage.getItem('artistList'));
  }
  
  onIdClick = (id) => {
    this.setState({
      idChosen: id,
      showModal: true
    })
  }

  separeteOneArtist = () => {
    const {artistList, idChosen} = this.state;

    if (!artistList) {
      return
    }

    let idx = artistList.findIndex( (item) => {
      return item.artist.artist_id === idChosen
    })

    return artistList[idx]
  }

  onModalClose = () => {
    this.setState({
      showModal: false
    })
  }

  setSearchValue = (searchValue) => {
    this.setState({
      searchValue
    })
  }

  searchItems = (array, searchValue) => {
    if (searchValue === 0 || !array) {
      return array;
    }

    return array.filter( (item) => {
      return item.artist.artist_name.toLowerCase().includes(searchValue.toLowerCase())
    })
  }

  render() {
    const {artistList, showModal, searchValue} = this.state;
    
    const artistToRender = this.searchItems(artistList, searchValue) ;
    const artistoModal = this.separeteOneArtist();

    return (
      <>
        <header>
          <Search onSearchChanged = {this.setSearchValue}
                  searchValue = {searchValue}/>
        </header>

        <main> 
          <ArtistsTable artistsList = {artistToRender}
                        onIdClick = {this.onIdClick} />
        </main>

        {showModal ? <ModalWindow closeModal = {this.onModalClose}
                                  artistData = {artistoModal}/> : null}
      </>
    );
  }
}

export default App;
