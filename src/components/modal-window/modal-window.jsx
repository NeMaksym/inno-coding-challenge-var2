import React, { Component } from 'react';

import './modal-window.css';

class ModalWindow extends Component {

    onModalClose = () => {
        const {closeModal} = this.props;
        closeModal();
    }

    render() {
        
        const {artist: {artist_id,
                        artist_name,
                        artist_country,
                        artist_rating,
                        artist_twitter_url,
                        updated_time}} = this.props.artistData;

        return (
            <>
                <div className="overlay"
                     onClick = {this.onModalClose}></div>

                <div className="modal">
                    <h3>Edit Dialog</h3>
                    <form className="modal__form">
                        <label htmlFor="id">ID:</label>
                        <input type="text" name="id" value = {artist_id} disabled/>

                        <label htmlFor="name">Name:</label>
                        <input type="text" name="name" value = {artist_name}/>

                        <label htmlFor="country">Country:</label>
                        <input type="text" name="country" value = {artist_country} disabled/>

                        <label htmlFor="rating">Rating:</label>
                        <input type="text" name="rating" value = {artist_rating}/>

                        <label htmlFor="uploaded">Uploaded at:</label>
                        <input type="text" name="uploaded" value = {updated_time} disabled/>

                        <label htmlFor="twitter">Twitter URL:</label>
                        <input type="text" name="twitter" value = {artist_twitter_url} disabled/>

                        <label htmlFor="comments">Comments:</label><br/>
                        <textarea name="comments" id="" cols="30" rows="10"></textarea>

                        <button>Cancel</button>
                        <button type="submit">Save</button>
                    </form>

                    <button className="close-btn"
                            onClick = {this.onModalClose}>X</button>
                </div>
            </>
        )
    }
}

export default ModalWindow;